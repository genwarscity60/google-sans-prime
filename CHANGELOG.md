### 2022-07-20
- Updated OMF template: Remember to install the latest AFDKO extension to benefit from all of its life improvement changes.
- Config: Rename option GS to GSOPSZ for less confusion. Removed deprecated options.

[Full Changelog](https://gitlab.com/nongthaihoang/google-sans-prime/-/commits/master)
